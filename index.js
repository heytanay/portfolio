const http = require('http');
const fs = require('fs');
const url = require('url');
const StringDecoder = require('string_decoder').StringDecoder;
const path = require('path');

var port = typeof(process.env.PORT) == 'number' ? process.env.PORT : 3000;

var home = fs.readFileSync("./views/home.html");
var about = fs.readFileSync("./views/about.html");
var projects = fs.readFileSync("./views/projects.html");

// Create the Server
var server = http.createServer(function(req,res){

    var pathName = url.parse(req.url, true).pathname;

    var queryString = url.parse(req.url, true).query;

    var trimmedPath = pathName.replace(/^\/+|\/+$/g,'');

    if (trimmedPath !== "home" && trimmedPath !== "about" && trimmedPath !== "projects"){
        res.writeHead(404);
        res.write("The File/Page you are looking for deosnot exist on this Server");
    }
    else if (trimmedPath == "home"){
        res.setHeader('Content-Type','text/html');
        res.writeHead(200);
        res.write(home);
    }
    else if (trimmedPath == "about"){
        res.setHeader('Content-Type','text/html');
        res.writeHead(200);
        res.write(about);
    }
    else if (trimmedPath == "projects"){
        res.setHeader('Content-Type','text/html');
        res.writeHead(200);
        res.write(projects);
    }

});

// Instantiate the Server
server.listen(port,function(){
    console.log("Server listening at PORT: ",port);
});

